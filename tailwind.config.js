const withMT = require("@material-tailwind/react/utils/withMT");
 
module.exports = withMT({
  content: ["./src/**/*.{js,jsx,ts,tsx}", "node_modules/flowbite-react/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      height: {
        '128': '32rem',
        '192': '48rem',
        '256': '64rem',
      },
      width: {
        '22': '5.3rem',
      }
    },
  },
  plugins: [require("daisyui"), require('flowbite/plugin')],
});