import { configureStore } from '@reduxjs/toolkit'
import connexionSlice from '../features/connexion/connexionSlice'
import alertInfoSlice from '../features/alert/alertInfoSlice'
import dashboardSlice from '../features/dashboard/dashboardSlice'
import articleSlice from '../features/article/articleSlice'
import filterSlice from '../features/filter/filterSlice'
import postsPerPageSlice from '../features/postsPerPage/postsPerPageSlice'

export default configureStore({
  reducer: {
    connexion: connexionSlice,
    alertInfo: alertInfoSlice,
    dashboard: dashboardSlice,
    article: articleSlice,
    filter: filterSlice,
    postsPerPage: postsPerPageSlice
  },
})