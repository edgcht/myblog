import IconConnexion from '../iconConnexion/IconConnexion';
import logo  from '../../logo.png'

function Header() {

  return (
    <header className="bg-white relative">
      <div>
        <nav className="mx-auto flex justify-end max-w-9xl p-3 lg:px-8 relative" aria-label="Global"> 
          <img src={logo} alt='' width={55} className='absolute left-0 top-2'/>
          <IconConnexion/>
        </nav>
      </div>
    </header>
  )
}

export default Header;