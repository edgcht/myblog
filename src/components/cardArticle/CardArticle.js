import React from 'react';
import parse from 'html-react-parser';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { doc, deleteDoc } from 'firebase/firestore';
import { setFilterCategory } from '../../features/filter/filterSlice';
import { dbT } from '../../App';
import { TrashIcon } from '@heroicons/react/20/solid';
import './style.css';

function CardArticle({ data, path, imageUrl }) {

  const { Author, Description, TimeToRead, Title, uid, Category } = data;
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const setCurrentArticle = () => {
    navigate(`/article/${uid}`);
  };

  const deletePost = async () => {
    try {
      await deleteDoc(doc(dbT, 'posts', uid));
      console.log('Post successfully deleted');
    } catch (error) {
      console.error('Error deleting post: ', error);
    }
  };

  return (
    <div className="card card-compact bg-base-100 shadow-xl">
      <figure className="relative">
      {path !== undefined && ( <span className="absolute text-xs p-1 text-white bg-red-700 rounded-md left-3 top-3 hover:cursor-pointer" onClick={deletePost}>                                
        <TrashIcon
          className="h-4 w-4"
          aria-hidden="true"
        /></span> )}
        <div className='max-h-[124.8px] min-h-[124.8px] overflow-hidden'>
          <img className='w-full min-h-[124.8px]' src={`${imageUrl != undefined && imageUrl != '' ? imageUrl : "https://jeromeobiols.com/wordpress/wp-content/uploads/photo-montagne-vallee-blanche-chamonix-mont-blanc.jpg"}`} alt={Title} />
        </div>
        <span className="absolute text-xs p-1 text-white bg-indigo-700 rounded-md right-3 top-3 hover:cursor-pointer" onClick={() => dispatch(setFilterCategory(Category[0]))}>
          {Category}
        </span>
      </figure>

      <div className="card-body hover:cursor-pointer flex flex-col justify-between" onClick={setCurrentArticle}>
        <div>
          <h2 className="font-semibold text-xl truncate pb-2">{Title}</h2>
          <div className="Description text-justify truncate-multiline-4 mb-4">{parse(Description)}</div>
        </div>
        <div className="card-actions justify-between">
          <span className="italic text-gray-600">{Author}</span>
          <span className="text-gray-600">{TimeToRead} min</span>
        </div>
      </div>
    </div>
  );
}

export default CardArticle;
