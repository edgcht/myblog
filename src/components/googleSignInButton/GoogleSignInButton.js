import React from 'react';
import { GoogleAuthProvider, getAuth } from "firebase/auth";
import { ConnexionGoogle } from '../../utils/oAuth/connexion-google';
import { useTranslation } from 'react-i18next';
import { alertSuccesfullyShow } from '../../features/alert/alertSuccesfullySlice';
import { useDispatch } from 'react-redux';
import { useNavigate } from "react-router-dom";

function GoogleSignInButton({ firebaseApp }) {

    const { t } = useTranslation();
    const auth = getAuth(firebaseApp);
    const navigate = useNavigate();

    const dispatch = useDispatch();

    async function handleGoogleConnexion() {
        try {
            await ConnexionGoogle(auth, new GoogleAuthProvider());
            dispatch(alertSuccesfullyShow('Connexion réussi, bon retour parmis nous'))
            navigate("/");
        } catch(error) {
            console.log('error : ', error);
        }
    }

    return (
        <button onClick={handleGoogleConnexion} type="button" className="inline-flex justify-center items-center space-x-2 border font-semibold rounded-lg px-3 py-2 leading-5 text-sm border-gray-200 bg-white text-gray-800 hover:border-gray-300 hover:text-gray-900 hover:shadow-sm focus:ring focus:ring-gray-300 focus:ring-opacity-25 active:border-gray-200 active:shadow-none dark:border-gray-700 dark:bg-gray-800 dark:text-gray-300 dark:hover:border-gray-600 dark:hover:text-gray-200 dark:focus:ring-gray-600 dark:focus:ring-opacity-40 dark:active:border-gray-700">
            <svg viewBox="0 0 48 48" width={16}>
                <title>Google Logo</title>
                <clipPath id="g">
                    <path d="M44.5 20H24v8.5h11.8C34.7 33.9 30.1 37 24 37c-7.2 0-13-5.8-13-13s5.8-13 13-13c3.1 0 5.9 1.1 8.1 2.9l6.4-6.4C34.6 4.1 29.6 2 24 2 11.8 2 2 11.8 2 24s9.8 22 22 22c11 0 21-8 21-22 0-1.3-.2-2.7-.5-4z"/>
                </clipPath>
                <g class="colors" clip-path="url(#g)">
                    <path fill="#FBBC05" d="M0 37V11l17 13z"/>
                    <path fill="#EA4335" d="M0 11l17 13 7-6.1L48 14V0H0z"/>
                    <path fill="#34A853" d="M0 37l30-23 7.9 1L48 0v48H0z"/>
                    <path fill="#4285F4" d="M48 48L17 24l-4-3 35-10z"/>
                </g>
            </svg>           
            <span>{t('google')}</span>
        </button>
    );
}

export default GoogleSignInButton;

