function SkeletonArticle() {

  return (
    <div className='h-[calc(100vh-68px)] bg-gray-100'>
        <div className="overflow-scroll h-full p-10">
            <div className="w-10/12 m-auto">
                <div className='flex items-end justify-between h-[88px]'>
                    <div className="skeleton h-20 w-8/12 pt-10"></div>
                    <div className='flex items-center'>
                        <div className='rounded-lg text-grey-600 text-xs italic flex items-center gap-3'>
                            <div className="skeleton h-4 w-40"></div>
                            <div className="skeleton w-16 h-16 rounded-full shrink-0"></div>
                        </div>
                    </div>
                </div>
                <div className='divider'></div>
                <div className='mt-10'>
                    <div className='flex justify-between items-center'>
                        <div className="skeleton h-4 w-40"></div>
                        <div className="skeleton h-4 w-56"></div>
                    </div>
                    <div className="flex flex-col gap-4 p-10 my-5 bg-white">
                        <div className="skeleton h-4 w-full"></div>
                        <div className="skeleton h-4 w-full"></div>
                        <div className="skeleton h-4 w-full"></div>
                        <div className="skeleton h-4 w-28"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  )
}

export default SkeletonArticle;