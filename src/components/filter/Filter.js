import React, { memo } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { TiDeleteOutline } from "react-icons/ti";
import { setFilterCategory, setFilterAuthor } from '../../features/filter/filterSlice';

function Filter() {

    const dispatch = useDispatch();

    const filterCategory = useSelector((state) => state.filter.filterCategory);
    const filterAuthor = useSelector((state) => state.filter.filterAuthor);

    const onClickCategory = (categoryToDelete) => {
        dispatch(setFilterCategory(categoryToDelete))
    }

    const onClickAuthor = (authorToDelete) => {
        dispatch(setFilterAuthor(authorToDelete))
    }

    return (
        <div>
            <div className='flex gap-2'>
                <>
                    {filterCategory && filterCategory.length > 0 &&
                        filterCategory.map((x, index) => (
                            <React.Fragment key={index}>
                                <span className='py-1 px-2 bg-indigo-700 rounded-lg text-white text-xs flex items-center gap-1'>
                                    {x}<TiDeleteOutline onClick={() => onClickCategory(x)} size={20} className='hover:cursor-pointer'/>
                                </span>
                            </React.Fragment>
                        ))
                    }

                    {/* Ajouter "et" si les deux tableaux ne sont pas vides */}
                    {filterCategory && filterCategory.length > 0 && filterAuthor && filterAuthor.length > 0 && (
                        <span className='mx-2 text-sm text-gray-600'>et</span>
                    )}

                    {filterAuthor && filterAuthor.length > 0 &&
                        filterAuthor.map((x, index) => (
                            <span key={index} className='py-1 px-2 bg-teal-500 rounded-lg text-white text-xs flex items-center gap-1'>
                                {x} <TiDeleteOutline onClick={() => onClickAuthor(x)} size={20} className='hover:cursor-pointer'/>
                            </span>
                        ))
                    }
                </>
            </div>
        </div>
    )
  }
  
  export default memo(Filter);