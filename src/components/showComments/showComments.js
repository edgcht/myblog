
import React, { useEffect, useState } from 'react';
import { dbT } from '../../App';
import { formatDateHours } from '../../utils/functions/function';
import parse from "html-react-parser";
import { useParams } from 'react-router-dom';
import {onSnapshot, collection, query, orderBy } from "firebase/firestore";
import LikeComments from '../likeComments/LikeComments';
import DislikeComments from '../dislikeComments/dislikeComments';


function ShowComments() {

    const { uid } = useParams();
    const [comments, setComments] = useState([]);


    useEffect(() => {
        const q = query(collection(dbT, 'posts', uid, 'comments'), orderBy('createdAt', 'desc'));

        return onSnapshot(q, (querySnapshot) => {
            let data = [];
            querySnapshot.forEach((doc) => {
                data.push(doc.data())
            });
            setComments(data.reverse())
        });
    }, [uid]); 

    return (
        <>
            <div className='container-comments'>
            {comments.length > 0 ? 
                comments.map(comment => 
                <div className='test px-6'>
                    <div className='py-6'>
                        <div className='flex justify-between'>
                            <span>{comment.Author} - <i>{formatDateHours(new Date(comment.Timestamp))}</i></span>
                            <div className='flex gap-5'>
                                <LikeComments comment={comment}/>
                                <DislikeComments comment={comment}/>
                            </div>    
                        </div>
                        <div className='divider mt-0 mb-1'></div>
                        <p>{parse(comment.Text)}</p>
                    </div>
                </div>) : 
                <h2 className='text-indigo-700 text-5xl font-bold'>0 commentaire</h2>}
            </div>
        </>
    )
}

export default ShowComments;