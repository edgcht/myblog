import React, { memo } from 'react';
import { PencilSquareIcon } from '@heroicons/react/20/solid';
import FormCreateArticle from '../formCreateArticle/FormCreateArticle';
import FormUpdateArticle from '../formUpdateArticle/FormUpdateArticle';

function Drawer({action, type, data}) {

    const typeAction = type;

    return(
        <>
        <div className="drawer drawer-end hover:cursor-pointer">
            <input id="my-drawer-4" type="checkbox" className="drawer-toggle absolute" />
            <div className="drawer-content">
                {/* Page content here */}
                <div className="toast toast-start">
                    <div className="tooltip tooltip-right hover:cursor-pointer " data-tip="Créer un nouvel article">
                        <div className="bg-white shadow-lg rounded-lg p-3 relative z-[999]" onClick={action}>
                            <label htmlFor="my-drawer-4" className="">
                                <PencilSquareIcon
                                    className="h-7 w-7 hover:cursor-pointer"
                                    aria-hidden="true"
                                />
                            </label>
                        </div>
                    </div>
                </div>

            </div> 
            <div className="drawer-side">
                <label htmlFor="my-drawer-4" aria-label="close sidebar" className="drawer-overlay"></label>
                <div className="menu w-6/12 min-h-full bg-base-200 text-base-content p-0">
                    { typeAction == 'update' ? 
                        <FormUpdateArticle data={data}/> :
                        <FormCreateArticle/>
                    }
                    
                </div>
            </div>
        </div>
        </>
    )
    
}

export default memo(Drawer);

