import { Menu, Transition } from '@headlessui/react';
import { Fragment } from 'react';
import { ChevronDownIcon, UserIcon, UserPlusIcon, UserMinusIcon } from '@heroicons/react/20/solid';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { getAuth, signOut } from "firebase/auth";
import { useNavigate } from "react-router-dom";

function IconConnexion() {

    const { t } = useTranslation();
    const auth = getAuth();
    const userLogin = useSelector(state => state.connexion.isLoggin);
    const navigate = useNavigate();

    const signOutUser = async () => {
        try {
            await signOut(auth);
            navigate('/');
        } catch (error) {
            console.error(error.message);
        }
    }

    const actions = [
        { label: userLogin ? t('Mon compte') : t('Se connecter'), icon: UserIcon, onClick: userLogin ? () => navigate('/') : () => navigate('/') },
        { label: userLogin ? t('Déconnexion') : t("S'inscrire"), icon: userLogin ? UserMinusIcon : UserPlusIcon, onClick: userLogin ? signOutUser : () => navigate('/sign-up') },
    ];
    
  return (
    <div className="text-right">
      <Menu as="div" className="relative inline-block text-left">
        <div className='hover:text-violet-100'>
          <Menu.Button className="inline-flex w-full justify-center rounded-md px-4 py-2 text-sm font-semibold leading-6 text-gray-900 hover:bg-opacity-30 focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75">
            <UserIcon
              className="ml-2 -mr-1 h-7 w-7"
              aria-hidden="true"
            />
            <ChevronDownIcon
              className="ml-1 -mr-3 h-7 w-7 text-gray-900"
              aria-hidden="true"
            />
          </Menu.Button>
        </div>
        <Transition
          as={Fragment}
          enter="transition ease-out duration-100"
          enterFrom="transform opacity-0 scale-95"
          enterTo="transform opacity-100 scale-100"
          leave="transition ease-in duration-75"
          leaveFrom="transform opacity-100 scale-100"
          leaveTo="transform opacity-0 scale-95"
        >
        <Menu.Items className="absolute z-[9999] right-0 mt-2 w-56 origin-top-right divide-y divide-gray-100 rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
            {actions.map(({ label, icon: Icon, onClick }, idx) => (
                <div key={idx} className="px-1 py-1">
                    <Menu.Item>
                        {({ active }) => (
                            <button
                                onClick={onClick}
                                className={`${
                                    active ? 'bg-violet-500 text-white' : 'text-gray-900'
                                    } group flex w-full items-center rounded-md px-2 py-2 text-sm`}
                            >
                                <Icon className="ml-1 mr-2 h-7 w-7 text-gray-900" aria-hidden="true" />
                                {label}
                            </button>
                        )}
                    </Menu.Item>
                </div>
            ))}
        </Menu.Items>
        </Transition>
      </Menu>
    </div>
  )
}

export default IconConnexion;