import React from 'react';
import { dbT } from '../../App';
import { useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';
import {collection, query, doc, where, getDocs, updateDoc} from "firebase/firestore";
import { AiOutlineLike } from "react-icons/ai";


function LikeComments({comment}) {

    const user = useSelector((state) => state.connexion.user);
    const { uid } = useParams();
    
    let username = JSON.parse(user).displayName;

    const handleClick = async () => {
        const commentsQuery = query(collection(dbT, 'posts', uid, 'comments'), where("Timestamp", "==", comment.Timestamp));
        const querySnapshot = await getDocs(commentsQuery);
        
        if (!querySnapshot.empty) {
            querySnapshot.forEach(async (documentSnapshot) => {

                const commentDocRef = doc(dbT, 'posts', uid, 'comments', documentSnapshot.id);

                // Récupère les données du commentaire actuel
                let currentComment = documentSnapshot.data();
                let decrementLike = currentComment.Like ? currentComment.Like - 1 : 1;
                let incrementLike = currentComment.Like ? currentComment.Like + 1 : 1;

                let updatedLikes;
                let queryToUpdate;

                if(currentComment.LikeBy){
                    let usernameIsLikeComment = currentComment.LikeBy.indexOf(username);
                    if(usernameIsLikeComment != -1){
                       currentComment.LikeBy.splice(usernameIsLikeComment, 1);
                       updatedLikes = decrementLike;
                       queryToUpdate = { Like: updatedLikes, LikeBy: currentComment.LikeBy}
                    }else{
                       currentComment.LikeBy.push(username)
                       updatedLikes = incrementLike;
                       queryToUpdate = { Like: updatedLikes, LikeBy: currentComment.LikeBy }
                    }
                }else{
                    updatedLikes = incrementLike;
                    queryToUpdate = { Like: updatedLikes, LikeBy: [username] }
                }

                try {
                    await updateDoc(commentDocRef, queryToUpdate)
                }catch(error) {
                    console.error("Error updating article: ", error);
                }
            });
        } else {
            console.log("No such document!");
        }        
    }

    return (
        <span className='flex items-center gap-1 hover:cursor-pointer' onClick={handleClick}><AiOutlineLike />{comment.Like}</span>
    )
}

export default LikeComments;