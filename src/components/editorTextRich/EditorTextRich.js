import React from 'react';
import { useField } from 'formik';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css'; //

function EditorTextRich({...props}) {

    const [field, , helpers] = useField(props);
    const { setValue } = helpers;
  
    const handleChange = (content, delta, source, editor) => {
      setValue(editor.getHTML()); // ou content pour juste le HTML
    };
  
    return (
      <ReactQuill theme="snow" value={field.value} onChange={handleChange} className=''/>
    );
}

export default EditorTextRich;