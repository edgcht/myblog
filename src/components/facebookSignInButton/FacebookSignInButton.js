import React from 'react';
import { FacebookAuthProvider, getAuth } from "firebase/auth";
import { connexionFacebook } from '../../utils/oAuth/connexion-facebook';
import { alertSuccesfullyShow } from '../../features/alert/alertSuccesfullySlice';
import { useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';
import { useNavigate } from "react-router-dom";


function FacebookSignInButton({ firebaseApp }) {

    const { t } = useTranslation();
    const auth = getAuth(firebaseApp);
    const navigate = useNavigate();

    const dispatch = useDispatch();

    async function handleFacebookConnexion() {
        try {
            await connexionFacebook(auth, new FacebookAuthProvider());
            dispatch(alertSuccesfullyShow('Connexion réussi, bon retour parmis nous'))
            navigate("/");
        } catch(error) {
            console.log(error);
        }
    }

    return (
        <button onClick={handleFacebookConnexion} type="button" className="inline-flex justify-center items-center space-x-2 border font-semibold rounded-lg px-3 py-2 leading-5 text-sm border-gray-200 bg-white text-gray-800 hover:border-gray-300 hover:text-gray-900 hover:shadow-sm focus:ring focus:ring-gray-300 focus:ring-opacity-25 active:border-gray-200 active:shadow-none dark:border-gray-700 dark:bg-gray-800 dark:text-gray-300 dark:hover:border-gray-600 dark:hover:text-gray-200 dark:focus:ring-gray-600 dark:focus:ring-opacity-40 dark:active:border-gray-700">
            <svg className="bi bi-facebook inline-block w-4 h-4 text-[#1877f2]" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 16 16" aria-hidden="true"><path d="M5.026 15c6.038 0 9.341-5.003 9.341-9.334 0-.14 0-.282-.006-.422A6.685 6.685 0 0 0 16 3.542a6.658 6.658 0 0 1-1.889.518 3.301 3.301 0 0 0 1.447-1.817 6.533 6.533 0 0 1-2.087.793A3.286 3.286 0 0 0 7.875 6.03a9.325 9.325 0 0 1-6.767-3.429 3.289 3.289 0 0 0 1.018 4.382A3.323 3.323 0 0 1 .64 6.575v.045a3.288 3.288 0 0 0 2.632 3.218 3.203 3.203 0 0 1-.865.115 3.23 3.23 0 0 1-.614-.057 3.283 3.283 0 0 0 3.067 2.277A6.588 6.588 0 0 1 .78 13.58a6.32 6.32 0 0 1-.78-.045A9.344 9.344 0 0 0 5.026 15z" /></svg>
            <span>{t('facebook')}</span>
        </button>
    );
}

export default FacebookSignInButton;

