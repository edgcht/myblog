import React from 'react';
import { dbT } from '../../App';
import { useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';
import {collection, query, doc, where, getDocs, updateDoc} from "firebase/firestore";
import { AiOutlineDislike } from "react-icons/ai";


function DislikeComments({comment}) {

    const user = useSelector((state) => state.connexion.user);
    const { uid } = useParams();

    let username = JSON.parse(user).displayName;

    const handleClick = async () => {
        const commentsQuery = query(collection(dbT, 'posts', uid, 'comments'), where("Timestamp", "==", comment.Timestamp));
        const querySnapshot = await getDocs(commentsQuery);
        
        if (!querySnapshot.empty) {
            querySnapshot.forEach(async (documentSnapshot) => {

                const commentDocRef = doc(dbT, 'posts', uid, 'comments', documentSnapshot.id);

                // Récupère les données du commentaire actuel
                let currentComment = documentSnapshot.data();
                let decrementLike = currentComment.Dislike ? currentComment.Dislike - 1 : 1;
                let incrementLike = currentComment.Dislike ? currentComment.Dislike + 1 : 1;

                let updatedLikes;
                let queryToUpdate;

                if(currentComment.DislikeBy){
                    let usernameIsLikeComment = currentComment.DislikeBy.indexOf(username);
                    if(usernameIsLikeComment != -1){
                        currentComment.DislikeBy.splice(usernameIsLikeComment, 1)
                        updatedLikes = decrementLike;
                        queryToUpdate = { Dislike: updatedLikes, DislikeBy: currentComment.DislikeBy}
                    }else{
                       currentComment.DislikeBy.push(username)
                       updatedLikes = incrementLike;
                       queryToUpdate = { Dislike: updatedLikes, DislikeBy: currentComment.DislikeBy }
                    }
                }else{
                    updatedLikes = incrementLike;
                    queryToUpdate = { Dislike: updatedLikes, DislikeBy: [username] }
                }

                try {
                    await updateDoc(commentDocRef, queryToUpdate)
                }catch(error) {
                    console.error("Error updating article: ", error);
                }

                // Crée une référence spécifique au document à partir de son ID
            });
        } else {
            console.log("No such document!");
        }        
    }


    return (
    <span className='flex items-center gap-1 hover:cursor-pointer' onClick={handleClick}><AiOutlineDislike />{comment.Dislike}</span>
    )
}

export default DislikeComments;