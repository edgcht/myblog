import React, { useEffect, useState } from "react";
import { useTranslation } from 'react-i18next';
import { useNavigate } from "react-router-dom";
import { alertShow } from '../../features/alert/alertInfoSlice';
import { alertSuccesfullyShow } from '../../features/alert/alertSuccesfullySlice';
import { useDispatch } from 'react-redux';
import { emailValide, inputNotEmptyValide } from '../../utils/functions/function'


function EmailMdpSignIn({ propsEmail, propsPassword }) {

    const { t } = useTranslation();
    const [emailIsValide, setEmailIsValide] = useState(false);
    const [passworIsdValide, setPasswordIsValide] = useState(false);
    
    const navigate = useNavigate();
    const dispatch = useDispatch();

    useEffect(() => {
      emailValide(propsEmail) ? setEmailIsValide(true) : setEmailIsValide(false);
      inputNotEmptyValide(propsPassword) ? setPasswordIsValide(true): setPasswordIsValide(false);
    }, [propsEmail, propsPassword])


    const handleSubmit = async (event) => {
      event.preventDefault();

      try {
        dispatch(alertSuccesfullyShow('Connexion réussi, bon retour parmis nous'));
        navigate("/");
      } catch (error) {
        dispatch(alertShow(error.message));
        console.error('Une erreur s’est produite lors de la connexion : ', error);
      }
    };

    return (
      <button disabled={ emailIsValide && passworIsdValide ? '' : true} onClick={handleSubmit} type="submit" className="w-full disabled:opacity-40 inline-flex justify-center items-center space-x-2 border font-semibold rounded-lg px-6 py-3 leading-6 bg-black bg-black text-white hover:text-white hover:bg-rose-200 hover:bg-black focus:ring focus:ring-blue-400 focus:ring-opacity-50 active:bg-blue-700 active:border-blue-700 dark:focus:ring-blue-400 dark:focus:ring-opacity-90">  
        <svg className="hi-mini hi-arrow-uturn-right inline-block w-5 h-5 opacity-50" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true"><path fillRule="evenodd" d="M12.207 2.232a.75.75 0 00.025 1.06l4.146 3.958H6.375a5.375 5.375 0 000 10.75H9.25a.75.75 0 000-1.5H6.375a3.875 3.875 0 010-7.75h10.003l-4.146 3.957a.75.75 0 001.036 1.085l5.5-5.25a.75.75 0 000-1.085l-5.5-5.25a.75.75 0 00-1.06.025z" clipRule="evenodd" /></svg>
        <span>Se connecter</span>
      </button>
    );
}

export default EmailMdpSignIn;

