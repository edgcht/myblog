
import React from 'react';
import { Formik, Form, ErrorMessage } from 'formik';
import EditorTextRich from '../editorTextRich/EditorTextRich';
import { useSelector } from 'react-redux';
import { collection, addDoc, serverTimestamp } from "firebase/firestore"; 
import { IoSend } from "react-icons/io5";
import { dbT } from "../../App"
import 'react-quill/dist/quill.snow.css'; 


const FormAddComment= ({id}) => {

    const userInfo = useSelector(state => state.connexion.user);
    let userInfoName = JSON.parse(userInfo).displayName;

    return (
      <Formik
        initialValues={{
          textRich: '',
        }}
          onSubmit={async (values, {resetForm}) => {

            const comment = {
                Author: userInfoName,
                Text: values.textRich,
                Timestamp: Date.now(),
                createdAt: serverTimestamp(),
                Like: 0,
                Dislike: 0
            };

            const commentsCollectionRef = collection(dbT, 'posts', id, 'comments');

            // Ajoutez un nouveau document à la sous-collection
            addDoc(commentsCollectionRef, comment)
            .then(docRef => {
                console.log("Document written with ID: ", docRef.id);
            }).catch(error => {
                console.error("Error adding document: ", error);
            });
          
          resetForm();
        }}
      >
        {({ isSubmitting }) => (
          <Form>
            <div className='flex flex-col'>
                <div className='w-full mt-6'>
                  <EditorTextRich name="textRich" />
                  <ErrorMessage name="textRich" component="div" className='text-rose-700'/>
                </div>

            </div>
            <button type="submit" disabled={isSubmitting} className="btn btn-outline btn-success ml-auto mb-10 mt-5">
              Créer
              <IoSend/>
            </button>
          </Form>
        )}
      </Formik>
    );
  };
  
  export default FormAddComment;