import React, { useState, useEffect } from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import EditorTextRich from '../editorTextRich/EditorTextRich';
import { useSelector } from 'react-redux';
import { doc, setDoc, collection, onSnapshot, query } from "firebase/firestore"; 
import { getStorage, ref as storageRef, uploadBytes, getDownloadURL } from "firebase/storage";
import { IoSend } from "react-icons/io5";
import { uid } from 'uid';
import { dbT } from "../../App"
import * as Yup from 'yup';
import 'react-quill/dist/quill.snow.css'; 

const validationSchema = Yup.object({
    title: Yup.string().required('Le titre est obligatoire'),
    description: Yup.string().required('La description est obligatoire').min(60, 'La description doit contenir au moins 60 caractères'),
    category: Yup.string().required('La catégorie est obligatoire'),
    timeToRead: Yup.string().required('La durée de lecture est obligatoire'),
    textRich: Yup.string().required('Le texte enrichi est obligatoire').min(255, 'Le texte enrichi doit contenir au moins 255 caractères'),
    file: Yup.mixed()
        .nullable(true) // Permet que la valeur soit null
        .test('fileSize', 'Le fichier est trop grand', 
            value => !value || (value && value.size <= 1024 * 1024)) // Par exemple, vérifie si la taille du fichier n'excède pas 1MB
        .test('fileType', 'Type de fichier non supporté', 
            value => !value || (value && ['image/jpeg', 'image/png'].includes(value.type))), // Exemple de vérification de type de fichier
});

const FormCreateArticle = () => {
    const userInfo = useSelector(state => state.connexion.user);
    const [categorys, setCategorys] = useState([]);
    let userInfoName = JSON.parse(userInfo).displayName;

    useEffect(() => {
      const ref = query(collection(dbT, 'categorys'));
  
      // Création et initialisation de l'abonnement Firestore
      const unsubscribe = onSnapshot(ref, (querySnapshot) => {
          const data = querySnapshot.docs.map(doc => doc.data());
          setCategorys(data);
      }, (error) => {
          console.error("Erreur lors de l'abonnement aux catégories : ", error);
      });
  
      // Fonction de nettoyage pour annuler l'abonnement lorsque le composant est démonté
      return () => {
          console.log('Unsubscribing from categorys');
          unsubscribe();
      };
  }, []); //

    return (
      <Formik
        initialValues={{
          title: '',
          description: '',
          category: '',
          file: '',
          timeToRead: '',
          textRich: '',
        }}
        validationSchema={validationSchema}
        onSubmit={async (values, {resetForm}) => {
            const uuid = uid(16)
            let fileUrl = ''; // Default value for file URL

            // Handling file upload
            if (values.file) {
                const fileRef = storageRef(getStorage(), `uploads/${uuid}/${values.file.name}`);
                await uploadBytes(fileRef, values.file);
                fileUrl = await getDownloadURL(fileRef);
            }

            const docData = {
              Title: values.title,
              Category: [values.category],
              CategorySimple: values.category,
              TimeToRead: values.timeToRead,
              Description: values.description,
              Text: values.textRich,
              Timestamp: Date.now(),
              uid: uuid,
              FileURL: fileUrl, // Add the URL of the file to your document
              Author: [userInfoName],
              AuthorSimple: userInfoName,
              AuthorCategory: [values.category, userInfoName]
            };

            values.file = null;
            await setDoc(doc(dbT, "posts", uuid), docData);
            resetForm();
        }}
      >
        {({ isSubmitting, setFieldValue }) => (
          <Form className='flex gap-5 flex-col justify-between items-start w-full'>

              <div className='w-full p-7 bg-indigo-700'>
                <span className='text-2xl block text-white font-bold'>Créer un article</span>
                <span className='text-lg block mt-1 mb-0 text-indigo-300'>Créez un nouvel article sur un sujet qui vous intéresse et laisser les gens vous donnez leur avis.</span>
              </div>

              <div className='flex gap-5 flex-col justify-between items-start w-full p-10'>
                <div className='w-full'>
                  <Field name="title" type="text" placeholder="Titre" className="w-full input input-bordered"/>
                  <ErrorMessage name="title" component="div" className='text-rose-700'/>
                </div>
    
                <div className='w-full'>
                  <Field name="description" as="textarea" placeholder="Description" className="w-full input input-bordered"/>
                  <ErrorMessage name="description" component="div" className='text-rose-700'/>
                </div>

                <div className='w-full'>
                  <Field as="select" name="category" className="w-full input input-bordered">
                    <option value="">Sélectionnez une catégorie</option>
                    {categorys.map((item, index) => (
                      <option key={index} value={item.name || item}>{item.name || item}</option>
                    ))}
                  </Field>
                  <ErrorMessage name="category" component="div" className='text-rose-700'/>
                </div>

                <div className='w-full'>
                  <Field name="timeToRead" type="number" placeholder="Durée de lecture" className="w-full input input-bordered"/>
                  <ErrorMessage name="timeToRead" component="div" className='text-rose-700'/>
                </div>

                <div className='w-full'>
                  <EditorTextRich name="textRich" />
                  <ErrorMessage name="textRich" component="div" className='text-rose-700'/>
                </div>

                <div className='w-full'>
                  <label htmlFor="file" className="block text-sm font-medium text-gray-700"></label>
                  <input id="file" name="file" type="file" onChange={(event) => {
                    setFieldValue("file", event.currentTarget.files[0]);
                  }} className="w-full input input-bordered"/>
                  <ErrorMessage name="file" component="div" className='text-rose-700'/>
                </div>

                <button type="submit" disabled={isSubmitting} className="btn btn-outline btn-success ml-auto relative top-12">
                  Créer <IoSend/>
                </button>
              </div>
          </Form>
        )}
      </Formik>
    );
};
  
export default FormCreateArticle;