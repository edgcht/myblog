import React, { useRef, memo, useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { collection, onSnapshot, query, orderBy } from "firebase/firestore"; 
import { dbT } from '../../App';
import { setFilterCategory, setFilterAuthor } from '../../features/filter/filterSlice';

function NavBar() {

    // const categorys = useSelector((state) => state.dashboard.categorys);
    // const authors = useSelector((state) => state.dashboard.authors);

    const [categorys, setCategorys] = useState([])
    const [authors, setAuthors] = useState([])

    const filterCategory = useSelector((state) => state.filter.filterCategory);
    const filterAuthor = useSelector((state) => state.filter.filterAuthor);

    const defaultCategory = 'Catégorie';
    const defaultAuthor = 'Auteur';

    const dispatch = useDispatch();

    const categoryRef = useRef();
    const authorRef = useRef();

    useEffect(() => {
        const ref = query(collection(dbT, 'posts'), orderBy('Title'));
    
        const unsubscribe = onSnapshot(ref, (querySnapshot) => {
            const categoriesData = querySnapshot.docs.map(doc => doc.data().Category);
            const authorsData = querySnapshot.docs.map(doc => doc.data().Author);
            setCategorys([...new Set(categoriesData.flat())]);
            setAuthors([...new Set(authorsData.flat())]);
        }, (error) => {
            console.error("Erreur lors de l'abonnement aux posts : ", error);
        });
    
        return () => {
            console.log('Unsubscribing from posts');
            unsubscribe();
        };
    }, []); // Ici, setCategorys et setAuthors sont utilisés comme dépendances.
    

    const handleChangeCategory = (e) => {
        let filterCategoryRef = e.target.value;
        if(filterCategoryRef !== 'Catégorie'){
            dispatch(setFilterCategory(filterCategoryRef));
        }
    }

    const handleChangeAuthor = (e) => {
        let filterAuthorRef = e.target.value;
        if(filterAuthorRef !== 'Auteur'){
            dispatch(setFilterAuthor(filterAuthorRef))
        }
        
    }

    return (
        <div className="navbar bg-base-100 mx-auto mb-16 rounded-[12px] shadow-lg">
            <div className="lg:flex w-full">
                <div className="flex justify-between items-center w-full">
                    <ul className="menu menu-horizontal px-1 flex gap-5">
                    <li>
                        <select className="select select-bordered w-full min-w-xs max-w-xs"  ref={categoryRef} onChange={(e) => handleChangeCategory(e)} value={filterCategory || defaultCategory}>
                            <option selected>Catégorie</option>
                            {
                                categorys.map(item => {
                                    return (
                                        <option value={item.name || item}>{item.name || item}</option>
                                    )
                                })
                            }
                        </select>
                    </li>
                    <li>
                        <select className="select select-bordered w-full min-w-xs max-w-xs"  ref={authorRef} onChange={(e) => handleChangeAuthor(e)} value={filterAuthor || defaultAuthor}>
                            <option selected>Auteur</option>
                            {
                                authors.map(item => {
                                    return (
                                        <option value={item}>{item.name || item}</option>
                                    )
                                })
                            }
                        </select>
                    </li>
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default memo(NavBar);