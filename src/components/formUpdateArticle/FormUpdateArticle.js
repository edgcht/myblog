import React, { useEffect, useState } from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import EditorTextRich from '../editorTextRich/EditorTextRich';
import { useSelector } from 'react-redux';
import { doc, updateDoc, collection, onSnapshot, query } from "firebase/firestore"; 
import { getStorage, ref as storageRef, uploadBytes, getDownloadURL } from "firebase/storage";
import { IoSend } from "react-icons/io5";
import { dbT } from "../../App";
import * as Yup from 'yup';
import 'react-quill/dist/quill.snow.css';

const validationSchema = Yup.object({
    title: Yup.string().required('Le titre est obligatoire'),
    description: Yup.string().required('La description est obligatoire').min(60, 'La description doit contenir au moins 60 caractères'),
    category: Yup.string().required('La catégorie est obligatoire'),
    timeToRead: Yup.string().required('La durée de lecture est obligatoire'),
    textRich: Yup.string().required('Le texte enrichi est obligatoire').min(255, 'Le texte enrichi doit contenir au moins 255 caractères'),
    file: Yup.mixed().nullable(true) // Le champ de fichier peut être nul
});

const FormUpdateArticle = ({ data }) => {
    const userInfo = useSelector(state => state.connexion.user);
    const [categorys, setCategorys] = useState([]);
    let userInfoName = JSON.parse(userInfo).displayName;

    useEffect(() => {
        const ref = query(collection(dbT, 'categorys'));
    
        // Création et initialisation de l'abonnement Firestore
        const unsubscribe = onSnapshot(ref, (querySnapshot) => {
            const data = querySnapshot.docs.map(doc => doc.data());
            setCategorys(data);
        }, (error) => {
            console.error("Erreur lors de l'abonnement aux catégories : ", error);
        });
    
        // Fonction de nettoyage pour annuler l'abonnement lorsque le composant est démonté
        return () => {
            console.log('Unsubscribing from categorys');
            unsubscribe();
        };
    }, []); //

    return (
        <Formik
            initialValues={{
                title: data.Title,
                description: data.Description,
                category: data.Category[0],
                timeToRead: data.TimeToRead,
                textRich: data.Text,
                file: null, // Initialisation du champ de fichier à null
            }}
            validationSchema={validationSchema}
            onSubmit={async (values, { resetForm, setFieldValue }) => {
                const articleRef = doc(dbT, "posts", data.uid);
                let updatedData = {
                    Title: values.title,
                    Category: [values.category],
                    CategorySimple: values.category,
                    TimeToRead: values.timeToRead,
                    Description: values.description,
                    Text: values.textRich,
                    Timestamp: Date.now(),
                    Author: [userInfoName],
                    AuthorSimple: userInfoName,
                    AuthorCategory: [values.category, userInfoName]
                };

                if (values.file) {
                    const fileRef = storageRef(getStorage(), `uploads/${data.uid}/${values.file.name}`);
                    await uploadBytes(fileRef, values.file);
                    const fileUrl = await getDownloadURL(fileRef);
                    updatedData.FileURL = fileUrl; // Mise à jour de l'URL du fichier
                }

                try {
                    await updateDoc(articleRef, updatedData);
                    console.log("Article updated successfully");
                    resetForm();
                } catch (error) {
                    console.error("Error updating article: ", error);
                }
            }}
        >
            {({ isSubmitting, setFieldValue }) => (
                <Form className='flex gap-5 flex-col justify-between items-start w-full'>
                    <div className='w-full p-7 bg-indigo-700'>
                        <span className='text-2xl block text-white font-bold'>Mettre à jour mon article</span>
                        <span className='text-lg block mt-1 mb-0 text-indigo-300'>Mettez à jour votre article pour corriger/modifier son contenu. Le tout en temps réel !</span>
                    </div>
                    <div className='flex gap-5 flex-col justify-between items-start w-full p-10'>
                        <div className='w-full'>
                            <Field name="title" type="text" placeholder="Titre" className="w-full input input-bordered"/>
                            <ErrorMessage name="title" component="div" className='text-rose-700'/>
                        </div>
                        <div className='w-full'>
                            <Field name="description" as="textarea" placeholder="Description" className="w-full input input-bordered"/>
                            <ErrorMessage name="description" component="div" className='text-rose-700'/>
                        </div>
                        <div className='w-full'>
                            <Field as="select" name="category" className="w-full input input-bordered">
                                <option value="">Sélectionnez une catégorie</option>
                                {categorys.map((item, index) => (
                                <option key={index} value={item.name || item}>{item.name || item}</option>
                                ))}
                            </Field>
                            <ErrorMessage name="category" component="div" className='text-rose-700'/>
                        </div>
                        <div className='w-full'>
                        <Field name="timeToRead" type="number" placeholder="Durée de lecture" className="w-full input input-bordered"/>
                        <ErrorMessage name="timeToRead" component="div" className='text-rose-700'/>
                    </div>

                    <div className='w-full'>
                        <EditorTextRich name="textRich" />
                        <ErrorMessage name="textRich" component="div" className='text-rose-700'/>
                    </div>

                    <div className='w-full'>
                        <label htmlFor="file" className="block text-sm font-medium text-gray-700"></label>
                        <input id="file" name="file" type="file" onChange={(event) => {
                            setFieldValue("file", event.currentTarget.files[0]);
                        }} className="w-full input input-bordered"/>
                        <ErrorMessage name="file" component="div" className='text-rose-700'/>
                    </div>

                    <button type="submit" disabled={isSubmitting} className="btn btn-outline btn-success ml-auto relative top-12">
                        Mettre à jour
                        <IoSend/>
                    </button>
                </div>
            </Form>
        )}
    </Formik>
  );
};

export default FormUpdateArticle;