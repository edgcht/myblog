import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import SignUpInLogin from './screens/SignUpInLogin';
import Dashboard from './screens/Dashboard';
import Header from './components/header/Header';
import { getAuth, setPersistence, browserLocalPersistence, onAuthStateChanged } from "firebase/auth";
import { initializeApp } from "firebase/app";
import { firebaseConfig } from './config/firebase/firebase-config';
import { login, logout, infoUser } from './features/connexion/connexionSlice';
import { useSelector, useDispatch } from 'react-redux';
import Page404 from './screens/Page404';
import { getFirestore } from "firebase/firestore";
import Article from './screens/Article';
import { getDatabase } from "firebase/database";
import MyPosts from './screens/MyPosts';

const app = initializeApp(firebaseConfig);
const database = getDatabase(app);
const dbT = getFirestore(app);

function App() {
  const dispatch = useDispatch();
  const userLogin = useSelector(state => state.connexion.isLoggin);   

  const auth = getAuth();
  setPersistence(auth, browserLocalPersistence);

  onAuthStateChanged(auth, user => {
    if(user === null){
      dispatch(logout());
    }else{
      dispatch(login());
      dispatch(infoUser(JSON.stringify(user)))
    }
  });

  return (
    <Router>
      <Header userLogin={userLogin} />
      <Routes>
        <Route path={"/"} element={ userLogin ? <Dashboard/> : <SignUpInLogin config={app} typeScreen={true}/>} />
        <Route path="sign-up" element={<SignUpInLogin config={app} typeScreen={false}/>} />
        <Route path="dashboard" element={ userLogin ? <Dashboard/> : <Page404/>} />
        <Route path="my-posts" element={ userLogin ? <MyPosts/> : <Page404/>} />
        <Route path="article/:uid" element={ userLogin ? <Article/> : <Page404/>} />
      </Routes>
    </Router>
  );
}

export { database, dbT }; 
export default App;