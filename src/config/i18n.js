import i18n from "i18next";
import { initReactI18next } from "react-i18next";

import translationEN from '../utils/locales/en/translation.json';
import translationFR from '../utils/locales/fr/translation.json';

// the translations
const resources = {
  en: {
    translation: translationEN
  },
  fr: {
    translation: translationFR
  }
};

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources,
    lng: "fr",

    keySeparator: false, 

    interpolation: {
      escapeValue: false 
    }
  });

export default i18n;
