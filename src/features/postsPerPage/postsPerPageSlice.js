import { createSlice } from '@reduxjs/toolkit';


export const postsPerPageSlice = createSlice({
  name: 'connexion',
  initialState: {
    postsPerPage: 50,
    postsPerPageTotal: 0,
    postsPerPageCurrent: 0,
  },
  reducers: {
    setPostsPerPage: (state, action) => {
        state.postsPerPage = action.payload
    },
    setPostsPerPageTotal: (state, action) => {
      state.postsPerPageTotal = action.payload
    },
    setPostsPerPageCurrent: (state, action) => {
      state.postsPerPageCurrent = action.payload
    },
  },
})

// Action creators are generated for each case reducer function
export const { setPostsPerPage, setPostsPerPageTotal, setPostsPerPageCurrent } = postsPerPageSlice.actions

export default postsPerPageSlice.reducer