import { createSlice } from '@reduxjs/toolkit';

export const alertSuccesfullySlice = createSlice({
  name: 'alert Succesfully',
  initialState: {
    value: false,
    message: null
  },
  reducers: {
    alertSuccesfullyShow: (state, action) => {
        state.value = true;
        state.message = action.payload; //Set error information when showing alert
    },
    alertSuccesfullyHide: (state) => {
      state.value = false;
      state.message = null; // Reset error information when hiding alert
    },
  },
});

// Action creators are generated for each case reducer function
export const { alertSuccesfullyShow, alertSuccesfullyHide } = alertSuccesfullySlice.actions;

export default alertSuccesfullySlice.reducer;
