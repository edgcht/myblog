import { createSlice } from '@reduxjs/toolkit';

export const alertInfoSlice = createSlice({
  name: 'alert',
  initialState: {
    value: false,
    error: null
  },
  reducers: {
    alertShow: (state, action) => {
      state.value = true;
      state.error = action.payload; // Set error information when showing alert
    },
    alertHide: (state) => {
      state.value = false;
      state.error = null; // Reset error information when hiding alert
    },
  },
});

// Action creators are generated for each case reducer function
export const { alertShow, alertHide } = alertInfoSlice.actions;

export default alertInfoSlice.reducer;
