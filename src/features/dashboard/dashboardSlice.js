import { createSlice } from '@reduxjs/toolkit';


export const dashboardSlice = createSlice({
  name: 'connexion',
  initialState: {
    posts: [],
    categorys: [],
    persistCategory: [],
    authors: []
  },
  reducers: {
    setPosts: (state, action) => {
        let data = action.payload
        const uniqueObjectsMap = new Map();

        // Utiliser l'ID de chaque objet comme clé pour le Map.
        data.forEach(obj => {
            uniqueObjectsMap.set(obj.id, obj);
        });

        const uniqueObjects = Array.from(uniqueObjectsMap.values());
        state.posts = uniqueObjects;
    },
    setCategorys: (state, action) => {
        state.categorys = action.payload
    },
    setAuthors: (state, action) => {
        state.authors = action.payload
    },
    setPersistCategory: (state, action) => {
      state.persistCategory = action.payload
    }, 
  },
})

// Action creators are generated for each case reducer function
export const { setPosts, setCategorys, setAuthors, setPersistCategory } = dashboardSlice.actions

export default dashboardSlice.reducer

// import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
// import { collection, getDocs, query, orderBy } from "firebase/firestore"; 
// import { dbT } from '../../App'; // Assurez-vous que ce chemin est correct

// // Async thunks
// export const fetchPosts = createAsyncThunk(
//     'dashboard/fetchPosts',
//     async () => {
//         const q = query(collection(dbT, "posts"), orderBy("Timestamp", "desc"));
//         const querySnapshot = await getDocs(q);
//         return querySnapshot.docs.map(doc => ({ id: doc.id, ...doc.data() }));
//     }
// );

// export const fetchCategorys = createAsyncThunk(
//     'dashboard/fetchCategorys',
//     async () => {
//         const q = query(collection(dbT, "categorys"));
//         const querySnapshot = await getDocs(q);
//         return querySnapshot.docs.map(doc => doc.data());
//     }
// );

// export const fetchAuthors = createAsyncThunk(
//     'dashboard/fetchAuthors',
//     async () => {
//         const q = query(collection(dbT, "authors"));
//         const querySnapshot = await getDocs(q);
//         return querySnapshot.docs.map(doc => doc.data());
//     }
// );

// // Dashboard slice
// export const dashboardSlice = createSlice({
//   name: 'dashboard',
//   initialState: {
//     posts: [],
//     categorys: [],
//     persistCategory: [],
//     authors: [],
//     status: 'idle', // Add loading state status
//   },
//   reducers: {},
//   extraReducers: (builder) => {
//     builder
//       .addCase(fetchPosts.fulfilled, (state, action) => {
//           state.posts = action.payload;
//       })
//       .addCase(fetchCategorys.fulfilled, (state, action) => {
//           state.categorys = action.payload;
//       })
//       .addCase(fetchAuthors.fulfilled, (state, action) => {
//           state.authors = action.payload;
//       });
//   },
// });

// export default dashboardSlice.reducer;
