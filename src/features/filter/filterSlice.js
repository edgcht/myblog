import { createSlice } from '@reduxjs/toolkit';


export const filterSlice = createSlice({
  name: 'connexion',
  initialState: {
    filterCategory: [],
    filterAuthor: [],
    filterTimeToRead: null,
    filterCurrentClick: null,
    lastFilterSelected: null
  },
  reducers: {
    setFilterCategory: (state, action) => {

      state.filterCurrentClick = 'Category';

      if(state.filterCategory === null){
        state.lastFilterSelected = 'Category';
      }
      else if(state.filterCategory === 'Category'){
        state.lastFilterSelected = null
      }

      if(!state.filterCategory.find((element) => element === action.payload)){
        state.filterCategory.push(action.payload);
      }else{
        state.filterCategory = state.filterCategory.filter(item => item !== action.payload);
      }


    },
    setFilterAuthor: (state, action) => {

      state.filterCurrentClick = 'Author';

      if(state.lastFilterSelected === null){
        state.lastFilterSelected = 'Author'
      }
      else if(state.lastFilterSelected === 'Author'){
        state.lastFilterSelected = null
      }

      if(!state.filterAuthor.find((element) => element === action.payload)){
        state.filterAuthor.push(action.payload);
      }else{
        state.filterAuthor = state.filterAuthor.filter(item => item !== action.payload);
      }
    },
    setFilterTimeToRead: (state, action) => {
      state.filterTimeToRead = action.payload;
    },
  },
})

// Action creators are generated for each case reducer function
export const { setFilterCategory, setFilterAuthor, setFilterTimeToRead } = filterSlice.actions

export default filterSlice.reducer