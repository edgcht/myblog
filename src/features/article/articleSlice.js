import { createSlice } from '@reduxjs/toolkit';


export const articleSlice = createSlice({
  name: 'article',
  initialState: {
    currentArticle: null,
  },
  reducers: {
    setArticle: (state, action) => {
        state.currentArticle = action.payload
    },
  },
})

// Action creators are generated for each case reducer function
export const { setArticle, setArticleToUpdate } = articleSlice.actions

export default articleSlice.reducer