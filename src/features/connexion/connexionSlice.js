import { createSlice } from '@reduxjs/toolkit';


export const connexionSlice = createSlice({
  name: 'connexion',
  initialState: {
    isLoggin: false,
    user: null
  },
  reducers: {
    login: (state) => {
      state.isLoggin = true
    },
    logout: (state) => {
      state.isLoggin = false
    },
    infoUser: (state, action) => {
      state.user = action.payload;
    },
  },
})

// Action creators are generated for each case reducer function
export const { login, logout, infoUser } = connexionSlice.actions

export default connexionSlice.reducer