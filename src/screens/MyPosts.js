import React, { useEffect, useState } from 'react';
import CardArticle from "../components/cardArticle/CardArticle";
import { useSelector } from 'react-redux';
import { dbT } from '../App';
import { collection, onSnapshot, query, where } from "firebase/firestore"; 

function MyPosts() {

    const [posts, setPosts] = useState([]);
    const currentUser = useSelector((state) => state.connexion.user);
    const username = JSON.parse(currentUser).displayName;

    useEffect(() => {
        const q = query(collection(dbT, "posts"), where('AuthorSimple', '==', username));
        return onSnapshot(q, (querySnapshot) => {
            const dataPosts = querySnapshot.docs.map(doc => ({ id: doc.id, ...doc.data() }));
            setPosts(dataPosts)
        });
    }, []);

    return (
        <div className='h-[calc(100vh-68px)] bg-gray-100'>
          <div className="overflow-scroll h-full p-10">
            <div className={posts.length > 0 ? "grid xl:grid-cols-5 lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-1 gap-10" : "flex items-center justify-center"}>
                  {posts.length > 0 ? 
                      posts.map(post => <CardArticle key={post.id} data={post} path={'myPosts'}/>) : 
                      <h2 className='text-indigo-700 text-5xl font-bold'>0 résultat</h2>}
              </div>
          </div>
        </div>
    )
  }

export default MyPosts;