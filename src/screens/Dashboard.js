import React, { useEffect } from 'react';
import CardArticle from "../components/cardArticle/CardArticle";
import NavBar from "../components/navBar/NavBar";
import { useDispatch, useSelector } from 'react-redux';
import { dbT } from '../App';
import { collection, onSnapshot, query, orderBy } from "firebase/firestore"; 
import { setPosts, setCategorys, setAuthors, setPersistCategory } from '../features/dashboard/dashboardSlice';
import { setPostsPerPage } from '../features/postsPerPage/postsPerPageSlice'
import Drawer from '../components/drawer/Drawer';
import Filter from '../components/filter/Filter';

function Dashboard() {

    const posts = useSelector((state) => state.dashboard.posts);
    const filterCategory = useSelector((state) => state.filter.filterCategory);
    const filterAuthor = useSelector((state) => state.filter.filterAuthor);
    const postPerPage = useSelector((state) => state.postsPerPage.postsPerPage);

    const dispatch = useDispatch();
    

    const loadNextPosts = () => {
        dispatch(setPostsPerPage(postPerPage + 50));
    }

  useEffect(() => {
      const q = query(collection(dbT, "posts"), orderBy("Timestamp", "desc"));

      onSnapshot(q, (querySnapshot) => {
          const dataPosts = querySnapshot.docs.map(doc => ({ id: doc.id, ...doc.data() }));
          const filteredPosts = dataPosts.filter(post => 
              (filterCategory.length === 0 || filterCategory.includes(post.CategorySimple)) &&
              (filterAuthor.length === 0 || filterAuthor.includes(post.AuthorSimple))
          );

          dispatch(setPosts(filteredPosts.slice(0, postPerPage)));
      });

  }, [dispatch, filterCategory, filterAuthor, postPerPage]);

  return (
      <div className='h-[calc(100vh-68px)] bg-gray-100'>
          <div className="overflow-scroll h-full p-10">
              <NavBar/>
              <Filter/>
              <div className="divider mb-5"></div>
              <div className={posts.length > 0 ? "grid xl:grid-cols-5 lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-1 gap-10" : "flex items-center justify-center"}>
                  {posts.length > 0 ? 
                      posts.map(post => <CardArticle key={post.id} data={post} imageUrl={post.FileURL}/>) : 
                      <h2 className='text-indigo-700 text-5xl font-bold'>0 résultat</h2>}
              </div>
              <div className="pagination flex justify-center mt-8">
                  <button onClick={loadNextPosts} className={'fixed bottom-5 right-5 bg-indigo-700 p-3 text-white rounded-md cursor-pointer hover:bg-indigo-500'}>Charger plus</button>
              </div>
              <Drawer/>
          </div>
      </div>
  );
}

export default Dashboard;