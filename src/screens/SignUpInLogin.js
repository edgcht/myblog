import '../App.css';
import React, { useState } from "react";
import EmailMdpSignUp from '../components/emailMdpSignUp/EmailMdpSignUp';
import EmailMdpSignIn from '../components/emailMdpSignIn/EmailMdpSignIn';
import GoogleSignInButton from '../components/googleSignInButton/GoogleSignInButton';
import FacebookSignInButton from '../components/facebookSignInButton/FacebookSignInButton';
import AlertError from '../components/alertError/AlertError';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import {
  Link
} from "react-router-dom";

function SignUpInLogin({config, typeScreen}) {

    const { t } = useTranslation();
    const alert = useSelector(state => state.alertInfo.value);

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    
    const handleChange = (e) => {
      const { target } = e;
        if(target.name === "email"){
            setEmail(target.value)
        } else if(target.name === "password"){
            setPassword(target.value)
        }
    };

    const translations = {
        title: typeScreen ? t('BibliApp') : t("S'inscrire"),
        description: typeScreen ? t('Se connecter') : t("S'inscrire"),
        text: typeScreen ? t("S'inscrire") : t("Se connecter"),
        textAccount: typeScreen ?  t('Pas de compte') : t('Déjà inscrit'),
    };

    const switchPageLink = typeScreen ? '/sign-up' : '/';
    const buttonLog = typeScreen ? 
        <EmailMdpSignIn firebaseApp={config} propsEmail={email} propsPassword={password}/> :
        <EmailMdpSignUp firebaseApp={config} propsEmail={email} propsPassword={password}/>

    return (
        <>  
        <div id="page-container" className="flex flex-col mx-auto w-full min-h-screen min-w-[320px] bg-gray-100 dark:text-gray-100 dark:bg-gray-900">
          <main id="page-content" className="flex flex-auto flex-col max-w-full">
            <div className="min-h-s creen flex items-center justify-center relative overflow-hidden max-w-10xl mx-auto p-4 lg:p-8 w-full">
              <section className="py-6 w-full max-w-xl">
                <header className="mb-10 text-center">
                  <h1 className="text-2xl font-bold inline-flex items-center mb-2 space-x-2">
                    <svg className="hi-mini hi-cube-transparent inline-block w-5 h-5 text-blue-600 dark:text-blue-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                      <path fillRule="evenodd" d="M9.638 1.093a.75.75 0 01.724 0l2 1.104a.75.75 0 11-.724 1.313L10 2.607l-1.638.903a.75.75 0 11-.724-1.313l2-1.104zM5.403 4.287a.75.75 0 01-.295 1.019l-.805.444.805.444a.75.75 0 01-.724 1.314L3.5 7.02v.73a.75.75 0 01-1.5 0v-2a.75.75 0 01.388-.657l1.996-1.1a.75.75 0 011.019.294zm9.194 0a.75.75 0 011.02-.295l1.995 1.101A.75.75 0 0118 5.75v2a.75.75 0 01-1.5 0v-.73l-.884.488a.75.75 0 11-.724-1.314l.806-.444-.806-.444a.75.75 0 01-.295-1.02zM7.343 8.284a.75.75 0 011.02-.294L10 8.893l1.638-.903a.75.75 0 11.724 1.313l-1.612.89v1.557a.75.75 0 01-1.5 0v-1.557l-1.612-.89a.75.75 0 01-.295-1.019zM2.75 11.5a.75.75 0 01.75.75v1.557l1.608.887a.75.75 0 01-.724 1.314l-1.996-1.101A.75.75 0 012 14.25v-2a.75.75 0 01.75-.75zm14.5 0a.75.75 0 01.75.75v2a.75.75 0 01-.388.657l-1.996 1.1a.75.75 0 11-.724-1.313l1.608-.887V12.25a.75.75 0 01.75-.75zm-7.25 4a.75.75 0 01.75.75v.73l.888-.49a.75.75 0 01.724 1.313l-2 1.104a.75.75 0 01-.724 0l-2-1.104a.75.75 0 11.724-1.313l.888.49v-.73a.75.75 0 01.75-.75z" clipRule="evenodd" />
                    </svg>
                    <span>{translations.description}</span>
                  </h1>
                  <h2 className="text-sm font-medium text-gray-500 dark:text-gray-400">
                    Bienvenue sur BibliApp
                  </h2>
                </header>
                <div className="flex flex-col rounded-lg shadow-sm bg-white overflow-hidden dark:text-gray-100 dark:bg-gray-800">
                  <div className="p-5 md:px-16 md:py-12 grow">
                    <form className="space-y-6">
                      <div className="space-y-1">
                        <label htmlFor="email" className="text-sm font-medium">{t('email')}</label>
                        <input onChange={handleChange} type="email" id="email" name="email" placeholder={t('Votre email')} className="w-full block border placeholder-gray-500 px-5 py-3 leading-6 rounded-lg border-gray-200 focus:border-rose-500 focus:ring focus:ring-rose-500 focus:ring-opacity-50 dark:bg-gray-800 dark:border-gray-600 dark:focus:border-rose-500 dark:placeholder-gray-400" />
                      </div>
                      <div className="space-y-1">
                        <label htmlFor="password" className="text-sm font-medium">{t('password')}</label>
                        <input onChange={handleChange} type="password" id="password" name="password" placeholder={t('Votre mot de passe')} className="w-full block border placeholder-gray-500 px-5 py-3 leading-6 rounded-lg border-gray-200 focus:border-rose-500 focus:ring focus:ring-border-500 focus:ring-opacity-50 dark:bg-gray-800 dark:border-gray-600 dark:focus:border-blue-500 dark:placeholder-gray-400" />
                      </div>
                      {alert ? <div className='w-full'><AlertError/></div> : <></>}
                      <div>
                        {buttonLog}
                        <div className="flex items-center my-5">
                          <span aria-hidden="true" className="grow bg-gray-100 rounded h-0.5 dark:bg-gray-700/75" />
                          <Link to={switchPageLink} className="text-xs font-medium text-gray-800 bg-gray-100 rounded-full px-3 py-1 dark:bg-gray-700 dark:text-gray-200">{translations.text}</Link>
                          <span aria-hidden="true" className="grow bg-gray-100 rounded h-0.5 dark:bg-gray-700/75" />
                        </div>
                        <div className="grid grid-cols-1 gap-2">
                          <GoogleSignInButton/>
                        </div>
                      </div>
                    </form>
                  </div>        
                  <div className="p-5 md:px-16 grow text-sm text-center bg-gray-50 dark:bg-gray-700/50">
                    {translations.textAccount} ? &nbsp;
                    <Link to={switchPageLink} className="font-medium text-rose-600 hover:text-rose-400 dark:text-rose-400 dark:hover:text-rose-300">{translations.text}</Link>
                  </div>
                </div>
              </section>
            </div>
          </main>
        </div>
      </>
    );
}

export default SignUpInLogin;