import parse from "html-react-parser";
import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import SkeletonArticle from "../components/skeletonArticle/SkeletonArticle";
import { dbT } from '../App';
import { useNavigate } from 'react-router-dom';
import { useParams } from 'react-router-dom';
import { formatDate } from '../utils/functions/function';
import { query, where, onSnapshot, collection, doc, deleteDoc } from "firebase/firestore";
import Drawer from '../components/drawer/Drawer';
import FormAddComment from "../components/formAddComment/FormAddComment";
import ShowComments from "../components/showComments/showComments";
import { FaRegUserCircle } from "react-icons/fa";


function Article() {

    const user = useSelector((state) => state.connexion.user);
    const username = JSON.parse(user).displayName;
    const userPicture = JSON.parse(user).photoURL;
    const [article, setArticle] = useState();
    const navigate = useNavigate();
    
    const { uid } = useParams();

    const deletePost = async () => {
        try {
          await deleteDoc(doc(dbT, 'posts', uid));
          navigate(-1);
          console.log('Post successfully deleted');
        } catch (error) {
          console.error('Error deleting post: ', error);
        }
    };

    useEffect(() => {
        const q = query(collection(dbT, "posts"), where("uid", "==", uid));

        return onSnapshot(q, (querySnapshot) => {
            querySnapshot.forEach((doc) => {
                setArticle(doc.data())
            });
        });
    }, [uid]); 


    return (
        <>
            {
                article ? (
                    <div className='bg-gray-100'>
                        <div className="max-h-96 overflow-hidden bg-blue-gray-500 lg:mb-10">
                            <img className="w-full" src={`${article.FileURL ? article.FileURL : 'https://jeromeobiols.com/wordpress/wp-content/uploads/photo-montagne-vallee-blanche-chamonix-mont-blanc.jpg' }`}/>
                        </div>
                        <div className="overflow-scroll p-10">
                            <div className="lg:w-10/12  md:w-11/12 m-auto">
                                <div className='flex lg:items-end justify-between lg:flex-row flex-col items-start gap-y-3'>
                                    <h2 className='text-5xl font-bold text-indigo-700 pt-10 lg:w-10/12 w-12/12'>
                                        {article.Title}
                                    </h2>
                                    <div className='flex items-center justify-end lg:w-2/12 w-12/12'>
                                        <div className='rounded-lg text-grey-600 text-xs italic flex items-center gap-3'>
                                            {article.Author}
                                            {
                                                article.Author[0] == username ? (
                                                    <img src={userPicture} alt={userPicture} width={36} className='rounded-full'/>
                                                ) : (
                                                    <FaRegUserCircle />
                                                )
                                            }
                                            
                                        </div>
                                    </div>
                                </div>
                                <div className='divider'></div>
                                <div className='mt-10'>
                                    <div className='flex justify-between lg:items-center lg:flex-row flex-col items-start gap-y-3'>
                                        <div className="flex gap-2 lg:items-center lg:flex-row flex-col items-start">
                                            <span className='bg-indigo-400 rounded-lg p-2 text-white text-xs mr-0'>{article.Category}</span>
                                            <span className='bg-amber-400 rounded-lg p-2 text-white text-xs mr-0'>{article.TimeToRead}min</span>
                                            {article.Author == username && (
                                                <details className="dropdown">
                                                    <summary className="m-1 btn bg-green-600 min-h-8 h-8 text-white hover:bg-green-300">Actions</summary>
                                                    <ul className="p-2 shadow menu dropdown-content z-[1] bg-base-100 rounded-box w-52">
                                                        <li><span onClick={deletePost}>Supprimer</span></li>
                                                    </ul>
                                                </details>
                                            )}
                                        </div>
                                        <span className='text-gray-600'>Publication : <span className='italic'>{formatDate(new Date(article.Timestamp))}</span></span>
                                    </div>
                                    <div className="Description text-justify bg-white p-10 my-5 basis-5/6">{parse(article.Text)}</div>
                                    {article.Author[0] == username && (
                                        <Drawer data={article} type={'update'}/>
                                    )}
                                </div>
                            </div>
                            <div className="w-10/12 m-auto">
                                <div className="mt-10 text-xl">Commentaires :</div>
                                <div className="divider"></div>
                                <div className="mt-6"></div>
                                <div className="m-auto">
                                    <ShowComments/>
                                    <FormAddComment id={uid}/>
                                </div>
                            </div>
                        </div>
                    </div>
                ) : (
                    <SkeletonArticle/>
                )
            }
        </>
    )
}

export default Article;