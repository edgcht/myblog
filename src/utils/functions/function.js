const emailValide = (valideEmail) => {
    // Définissez l'expression régulière pour le format de l'email
    var regexEmail = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;

    // Utilisez la méthode test() de l'Objet RegExp pour tester la validité de l'email
    return regexEmail.test(valideEmail);
}

const passwordValide = (password) => {
    const lengthRequirement = password.length >= 8;
    const uppercaseRequirement = /[A-Z]/.test(password);
    const specialCharacterRequirement = /[!@#$%^&*(),.?":{}|<>]/.test(password);

    return lengthRequirement && uppercaseRequirement && specialCharacterRequirement;
}

const inputNotEmptyValide = (inputChamp) => {
    // Supprime les espaces au début et à la fin du champ
    inputChamp = inputChamp.trim();

    // Vérifie si le champ est vide
    return !(inputChamp === null || inputChamp === "");
}

const showErrorPassword = (motDePasse) => {
    let countValide = 0;

    if(motDePasse.length === 0){
        return countValide = -4;
    }

    if (motDePasse.length < 8) {
        console.log("Erreur: Le mot de passe doit contenir au moins 8 caractères.");
        countValide--;
    }

    if (!/[A-Z]/.test(motDePasse)) {
        console.log("Erreur: Le mot de passe doit contenir au moins une majuscule.");
        countValide--;
    }

    if (!/[!@#$%^&*()_+\-={}[];':"\\|,.<>?]+/.test(motDePasse)) {
        console.log("Erreur: Le mot de passe doit contenir au moins un caractère spécial.");
        countValide--;
    }

    return countValide--;
}

const formatDate = (date) => {
    let day = date.getDate().toString().padStart(2, '0');
    let month = (date.getMonth() + 1).toString().padStart(2, '0'); // getMonth() retourne un mois de 0 à 11
    let year = date.getFullYear();

    return `${day}/${month}/${year}`;
}

const formatDateHours = (date) => {
    let day = date.getDate().toString().padStart(2, '0');
    let month = (date.getMonth() + 1).toString().padStart(2, '0'); // getMonth() retourne un mois de 0 à 11
    let year = date.getFullYear();
    let hours = date.getHours().toString().padStart(2, '0');
    let minutes = date.getMinutes().toString().padStart(2, '0');
    let seconds = date.getSeconds().toString().padStart(2, '0');

    return `${day}/${month}/${year} ${hours}:${minutes}:${seconds}`;
};

export { emailValide, passwordValide, inputNotEmptyValide, showErrorPassword, formatDate, formatDateHours }
