import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchPosts } from './features/posts/postsSlice';

const PostsComponent = () => {
    const dispatch = useDispatch();
    const posts = useSelector((state) => state.posts.items);
    const status = useSelector((state) => state.posts.status);
    const error = useSelector((state) => state.posts.error);

    useEffect(() => {
        if (status === 'idle') {
            dispatch(fetchPosts());
        }
    }, [status, dispatch]);

    // Gestion de l'affichage en fonction de l'état de la requête
    if (status === 'loading') {
        return <div>Chargement...</div>;
    } else if (status === 'succeeded') {
        return (
            <ul>
                {posts.map(post => (
                    <li key={post.id}>{post.title}</li>
                ))}
            </ul>
        );
    } else if (status === 'failed') {
        return <div>Erreur: {error}</div>;
    }

    return <div>Posts component</div>;
};
